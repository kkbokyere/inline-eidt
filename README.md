# Coyote App Test

## Introduction

The front-end code for the Coyote test lending app. Built in [React](https://reactjs.org/).

## Requirements

- [Node](https://nodejs.org/en/).
- [npm](https://www.npmjs.com/package/npm).

## Getting Started

**1. Clone Git Repo.**

```
$ git clone git clone https://kkbokyere@bitbucket.org/kkbokyere/inline-eidt.git
```

**2. Install Dependencies.**

Once that's all done, cd into the coyote-ui-interview/inline-edit directory and install the depedencies:

```
$ cd coyote-ui-interview/inline-edit
$ yarn install
```

**3. Run Application.**

Once the node modules have all been installed and npm has done it's thing, that's it. To open up a local development environment, run:

```
$ yarn start
```

Once the server is up and running, navigate to [localhost:3000](http://localhost:3000).

## Testing

[Jest](https://jestjs.io/) is the test runner used, with [React Testing Library](https://testing-library.com/docs/react-testing-library/) is testing library used for testing components. To run test use the following command:

```
$ yarn test
```

## Deployment

No CI/CD pipeline at the moment.

# Tools Used

- [React](https://reactjs.org/)
- [Create React App](https://create-react-app.dev/)
- [CSS Modules](https://github.com/css-modules/css-modules)


# Improvements / Retrospective Review

- I chose not to use Redux, as its only a small App. If the app became bigger and required the use of a more global state, then i would have added it.
- If you want to produce a failure in API just change the isSuccess param in the api/submitTextApi.js

- Would have used Cypress for E2E testing
- 100% test coverage
- create a better Error handler - only catching one simple error scenario
- implement typescript to better type definitions
- improve rendering state by using useMemo or useCallback
- better styling
