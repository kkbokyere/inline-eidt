import React, {useState} from 'react';
import styles from './App.module.scss'
import Form from "./components/Form";
import submitTextApi from "./api/submitTextApi";
import LoadingSpinner from "./components/LoadingSpinner";
import StatusIcon from "./components/StatusIcon";

function App() {
  const [inputText, setInputText] = useState('');
  const [errorMessage, setErrorMessage] = useState('');
  const [isLoading, setLoading] = useState(false);
  const [statusIcon, setStatusIcon] = useState('');
  const handleSubmit = async (postData) => {
    let response;
    setLoading(true);
    try {
      response = await submitTextApi(postData);
      setStatusIcon('success');
      setInputText(response.data);
      setTimeout(() => {
        setStatusIcon('')
      }, 1000)
    } catch (e) {
      setErrorMessage(e.message);
      setStatusIcon('failure');
      setTimeout(() => {
        setStatusIcon('');
        setErrorMessage('');
      }, 1000)
    }
    setLoading(false)
  };
  return (
    <div className={styles.App}>
      <h1>Inline Edit</h1>
      <div className={styles.inputContainer}>
        <Form isLoading={isLoading} handleOnSubmit={handleSubmit} inputText={inputText}/>
        <div>
          <StatusIcon type={statusIcon}/>
          <LoadingSpinner isLoading={isLoading}/>
        </div>
      </div>
      <div className={styles.errorContainer}>
        <span className={styles.errorMessage}>{errorMessage}</span>
      </div>
    </div>
  );
}

export default App;
