import React from 'react';
import { render, fireEvent } from '@testing-library/react';
import App from './App';

describe('App Tests', () => {
  it('renders App', () => {
    const { getByText, getByTestId } = render(<App/>);
    const linkElement = getByText("Inline Edit");
    const searchInput = getByTestId("text-input");
    expect(linkElement).toBeInTheDocument();
    expect(searchInput).toBeInTheDocument();
  });

  it('should displays current value in input', () => {
    const { getByTestId } = render(<App/>);
    const searchInput = getByTestId("text-input");
    expect(searchInput.value).toBe('');
    fireEvent.change(searchInput, { target: { value: "london"}});
    expect(searchInput.value).toBe('london')
  });

  it('should submit form', () => {
    const { getByTestId } = render(<App/>);
    const editForm = getByTestId("edit-form");
    const searchInput = getByTestId("text-input");
    fireEvent.change(searchInput, { target: { value: "london"}});
    fireEvent.submit(editForm);
    expect(searchInput.value).toBe('london')
  });
});
