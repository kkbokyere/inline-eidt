export default (data, isSuccess = true) => {
    return new Promise((resolve, reject) => {
        if (isSuccess) {
            setTimeout(() => resolve({
                status: 'success',
                data
            }), 1000)
        } else {
            reject(new Error("Something went wrong"))
        }
    })
};
