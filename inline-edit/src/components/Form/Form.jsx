import React, {useState} from 'react';
import PropTypes from 'prop-types';
import styles from './Form.module.scss'

const Form = ({ handleOnSubmit, inputText, isLoading }) => {
    const [searchTerm, setSearchTerm] = useState('');
    const onSubmitForm = (e) => {
        handleOnSubmit(searchTerm);
        e.preventDefault();
    };
    const handleOnChange = (e) => {
        setSearchTerm(e.currentTarget.value)
    };
    return(
        <form onSubmit={onSubmitForm} data-testid="edit-form" onBlur={onSubmitForm}>
            <input
                disabled={isLoading}
                defaultValue={inputText}
                data-testid="text-input"
                onChange={handleOnChange}
                className={styles.inputBase}
                placeholder="Enter text here"
            />
        </form>
    )
};

Form.defaultProps =  {
    handleOnSubmit: () => {}
};
Form.propTypes = {
    handleOnSubmit: PropTypes.func,
    inputText: PropTypes.string,
    isLoading: PropTypes.bool
};

export default Form;
