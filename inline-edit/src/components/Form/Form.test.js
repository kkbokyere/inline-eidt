import React from 'react';
import { render, fireEvent } from '@testing-library/react';
import Form from './Form';

describe('Form Tests', () => {
    it('should show static value', () => {
        const { getByTestId } = render(<Form inputText="test" />);
        const searchInput = getByTestId("text-input");
        expect(searchInput.value).toBe('test');
    });

    it('should disable input when loading', () => {
        const { getByTestId } = render(<Form isLoading={true} />);
        const searchInput = getByTestId("text-input");
        expect(searchInput).toBeDisabled();
    });

    it('should call handleSubmit form', () => {
        const handleSubmitMock = jest.fn();
        const { getByTestId } = render(<Form handleOnSubmit={handleSubmitMock} />);
        const editForm = getByTestId("edit-form");
        const searchInput = getByTestId("text-input");
        fireEvent.change(searchInput, { target: { value: "london"}});
        fireEvent.submit(editForm);
        expect(searchInput.value).toBe('london');
        expect(handleSubmitMock).toHaveBeenCalledWith('london')
    });
});
