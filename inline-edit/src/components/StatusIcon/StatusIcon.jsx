import React from 'react';
import cx from 'classnames'
import styles from './StatusIcon.module.scss'
import PropTypes from 'prop-types';

const StatusIcon = ({ type }) => {
    const btnClass = cx({
        [styles.ggCheck]: type === 'success',
        [styles.ggClose]: type === 'failure'
    });
    return <i className={btnClass}/>
};
StatusIcon.propTypes = {
    type: PropTypes.oneOf(['success', 'failure', ''])
};

export default StatusIcon;
